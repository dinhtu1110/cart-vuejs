import Vue from 'vue'
import Router from 'vue-router'


import ListProducts from '@/components/ListProducts'
import Dathang from '@/components/Dathang.vue'

Vue.use(Router)
import axios from 'axios';

Vue.prototype.$http = axios;

export default new Router({
  routes: [
    {
      path: '/',
      name: 'listproducts',
      component: ListProducts
    },
    {
      name: 'Dathang',
      path: '/dathang',
      component: Dathang
    }
  ]
})
