// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueCsrf from 'vue-csrf';

Vue.use(VueCsrf);

import router from './router'


import axios from 'axios'

Vue.use(axios)
import VueSession from 'vue-session'
Vue.use(VueSession)


//Vue.prototype.$axios = axios
Vue.use({
  install (Vue) {
    let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMzU3IiwibmFtZSI6IkNodW5nIExcdTAwZWEgXHUwMTEwXHUxZWU5YyIsImZhY2Vib29rX2lkIjoiIiwidGltZXpvbmUiOiI3IiwiaW50ZXJlc3RlZCI6IiIsImdlbmRlciI6Ik0iLCJiaXJ0aGRheSI6IjAwMDAtMDAtMDAiLCJyZWdpb24iOiJWTiIsImF2YXRhciI6Imh0dHBzOlwvXC9saDUuZ29vZ2xldXNlcmNvbnRlbnQuY29tXC8tUVJuU0JTVlpTQnNcL0FBQUFBQUFBQUFJXC9BQUFBQUFBQUFyQVwvRFBuakttVjhVcTRcL3Bob3RvLmpwZyIsInR5cGUiOiIwIiwiZW1haWwiOiJzaG9vdGluZ3N0YXIubGRjQGdtYWlsLmNvbSJ9.9eSp8gZouo8Mx_5y5-027DII1yhs73qjCSKvQKWTquA';
    Vue.prototype.$axios = axios.create({
      baseURL: 'http://local.vuejs.com/',
      headers: {"Authorization" : `Bearer ${token}`}
    })
  }
})

Vue.config.productionTip = false


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
